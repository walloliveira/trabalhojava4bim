package teste.de.insercao;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import br.unicesumar.dao.impl.ComandaDAO;
import br.unicesumar.dao.impl.ItemComandaDAO;
import br.unicesumar.dao.impl.MesaDAO;
import br.unicesumar.dao.impl.ProdutoDAO;
import br.unicesumar.persistence.Bebida;
import br.unicesumar.persistence.Comanda;
import br.unicesumar.persistence.ItemComanda;
import br.unicesumar.persistence.Mesa;
import br.unicesumar.persistence.Porcao;
import br.unicesumar.persistence.Produto;
import br.unicesumar.types.NumeroDaMesa;
import br.unicesumar.types.StatusDaMesa;
import br.unicesumar.types.TipoDeBebida;
import br.unicesumar.types.TipoDePorcao;

public class Insercao {

	// @Test
	// public void testInserirBebida() {
	// Bebida p = new Bebida();
	// p.setNome("cerveja");
	// p.setPreco(5.50);
	// p.setTipoBebida(TipoDeBebida.COM_ALCOOL);
	// ProdutoDAO dao = new ProdutoDAO();
	// dao.save(p);
	// }
	//
	// @Test
	// public void testInserirPorcao() {
	// Porcao p = new Porcao();
	// p.setNome("Batata");
	// p.setPreco(7.90);
	// p.setTipoDePorcao(TipoDePorcao.FRITOS);
	// ProdutoDAO dao = new ProdutoDAO();
	// dao.save(p);
	// }
	//
	// @Test
	// public void testMesa() {
	// Mesa m = new Mesa();
	// m.setStatus(StatusDaMesa.LIVRE);
	// m.setNumeroMesa(NumeroDaMesa.MESA_02);
	// MesaDAO mDao = new MesaDAO();
	// mDao.save(m);
	// }

	// @Test
	// public void testPesquisaBebida() {
	// ProdutoDAO pi = new ProdutoDAO();
	// assertEquals(1, pi.pesquisarBebida("cerveja").size());
	// }

	// @Test
	// public void testPesquisaPorcao() {
	// ProdutoDAO pi = new ProdutoDAO();
	// assertEquals(0, pi.pesquisarPorcao("frango").size());
	// }

	// @Test
	// public void testPesquisaMesa() {
	// MesaDAO dao = new MesaDAO();
	// assertEquals(2, dao.pesquisarMesa("01"));
	// }

	@Test
	public void testInsercaoComandaEItemComanda() {
		Comanda cm = new Comanda();
		ItemComanda icm = new ItemComanda();
		Produto prod = new ProdutoDAO().findByID(10L);
		icm.setProduto(prod);
		icm.setComanda(cm);
		List<ItemComanda> listaItem = new ArrayList<ItemComanda>();
		listaItem.add(icm);
		cm.setItensComanda(listaItem);
		ComandaDAO dao = new ComandaDAO();
		dao.save(cm);
	}
}
