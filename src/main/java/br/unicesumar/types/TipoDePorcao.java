package br.unicesumar.types;

public enum TipoDePorcao {
	FRIOS("Frios"), FRITOS("Fritos"), ASSADOS("Assados"), OUTROS("Outros");
	private String descricao;

	TipoDePorcao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return this.descricao;
	}
}
