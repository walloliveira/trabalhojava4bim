package br.unicesumar.types;

public enum StatusDaComanda {
	ABERTA("Aberta"), Fechada("Fechada");
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	private StatusDaComanda(String descricao) {
		this.descricao = descricao;
	}

}
