package br.unicesumar.types;

public enum TipoDeBebida {
	COM_ALCOOL("Alcolico"), SEM_ALCOOL("Sem Alcool");
	private String descricao;

	TipoDeBebida(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
