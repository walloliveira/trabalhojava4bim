package br.unicesumar.types;

public enum StatusDaMesa {
	OCUPADO("Ocupado"), LIVRE("Livre");
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	private StatusDaMesa(String descricao) {
		this.descricao = descricao;
	}
}
