package br.unicesumar.types;

public enum StatusDoPedido {
	EM_ANDAMENTO("Em Andamento"), ENTREGUE("Entregue"), PAGO("Pago");
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	private StatusDoPedido(String descricao) {
		this.descricao = descricao;
	}

}
