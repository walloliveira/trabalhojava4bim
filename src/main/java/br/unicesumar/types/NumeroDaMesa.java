package br.unicesumar.types;

public enum NumeroDaMesa {
	MESA_01("01"), MESA_02("02"), MESA_03("03"), MESA_04("04"), MESA_05("05");
	private String descricao;

	private NumeroDaMesa(String descricao) {
		this.descricao = descricao;
	}

	private String getDescricao() {
		return descricao;
	}

}
