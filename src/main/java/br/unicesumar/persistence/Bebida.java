package br.unicesumar.persistence;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import br.unicesumar.types.TipoDeBebida;

@Entity
@DiscriminatorValue("B")
public class Bebida extends Produto {

	@Column(name = "tipo", nullable = false)
	@Enumerated(EnumType.STRING)
	private TipoDeBebida tipoBebida;

	public void setTipoBebida(TipoDeBebida tipoBebida) {
		this.tipoBebida = tipoBebida;
	}

	public TipoDeBebida getTipoBebida() {
		return tipoBebida;
	}
}
