package br.unicesumar.persistence;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;

@Entity
public class Comanda {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private String numero;

	@Column
	private String data;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="comanda")
	private List<ItemComanda> itensComanda;

	public List<ItemComanda> getItensComanda() {
		return itensComanda;
	}

	public void setItensComanda(List<ItemComanda> itensComanda) {
		this.itensComanda = itensComanda;
	}

	@Column
	private String hora;

	public Comanda() {
		this.data = dateComanda();
		this.hora = horaComanda();
	}

	public long getId() {
		return id;
	}

	public String getData() {
		return data;
	}

	public String getHora() {
		return hora;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	private String dateComanda() {
		SimpleDateFormat dataFormat = new SimpleDateFormat("dd/MM/YYYY");
		Date data = new Date();
		return dataFormat.format(data);
	}

	private String horaComanda() {
		SimpleDateFormat dataFormat = new SimpleDateFormat("HH:mm:ss");
		Date data = new Date();
		return dataFormat.format(data);
	}
}
