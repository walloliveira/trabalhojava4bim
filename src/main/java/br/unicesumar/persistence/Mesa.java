package br.unicesumar.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.unicesumar.types.StatusDaMesa;

@Entity
public class Mesa {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_mesa")
	private Long id;

	@Enumerated(EnumType.STRING)
	private StatusDaMesa status = StatusDaMesa.LIVRE;

	@Column(nullable = false, length = 5)
	private String numeroMesa;

	public String getNumeroMesa() {
		return numeroMesa;
	}

	public void setNumeroMesa(String numeroMesa) {
		this.numeroMesa = numeroMesa;
	}

	public void setStatus(StatusDaMesa status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public Mesa() {
	}

	public StatusDaMesa getStatus() {
		return status;
	}

}
