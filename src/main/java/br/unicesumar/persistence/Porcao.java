package br.unicesumar.persistence;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import br.unicesumar.types.TipoDePorcao;

@Entity
@DiscriminatorValue("P")
public class Porcao extends Produto {

	@Column(name = "tipo", nullable = false)
	@Enumerated(EnumType.STRING)
	private TipoDePorcao tipoDePorcao;

	public TipoDePorcao getTipoDePorcao() {
		return tipoDePorcao;
	}

	public void setTipoDePorcao(TipoDePorcao tipoDePorcao) {
		this.tipoDePorcao = tipoDePorcao;
	}

}
