package br.unicesumar.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.unicesumar.dao.impl.MesaDAO;
import br.unicesumar.persistence.Mesa;
import br.unicesumar.types.NumeroDaMesa;

@ManagedBean
@SessionScoped
public class MesaBean {
	private Mesa mesa = new Mesa();
	private List<Mesa> listagemMesa = new ArrayList<Mesa>();
	private MesaDAO dao = new MesaDAO();
	private String numero;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String inserirMesa() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			dao.save(mesa);
			mesa = new Mesa();
			context.addMessage(null, new FacesMessage("Inserido com sucesso"));
		} catch (Exception e) {
			context.addMessage(null,
					new FacesMessage("Erro ao inserir" + e.getMessage()));
		}
		return "mesaList";
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	public Mesa getMesa() {
		return mesa;
	}
	
	public void pesquisarMesa() {
		listagemMesa = dao.pesquisarMesa(numero);
	}

	public List<Mesa> getListagemMesa() {
		return listagemMesa;
	}

	public void setListagemMesa(List<Mesa> listagemMesa) {
		this.listagemMesa = listagemMesa;
	}

	public String getNumeroDaMesa() {
		return this.mesa.getNumeroMesa();
	}

	public String novaMesa() {
		this.mesa = new Mesa();
		return "mesas";
	}

	public String editarMesa(Mesa mesa) {
		this.mesa = mesa;
		return "mesas";
	}

	public void guardarSelecaoMesa(Mesa mesa) {
		this.mesa = mesa;
	}
}
