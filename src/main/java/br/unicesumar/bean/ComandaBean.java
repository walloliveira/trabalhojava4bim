package br.unicesumar.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.unicesumar.persistence.Comanda;


@ManagedBean
@SessionScoped
public class ComandaBean {
	private Comanda comanda = new Comanda();
	private List<Comanda> listagemComanda = new ArrayList<Comanda>();
	
	public Comanda getComanda() {
		return comanda;
	}
	
	public void setComanda(Comanda comanda) {
		this.comanda = comanda;
	}
	
	public List<Comanda> getListagemComanda() {
		return listagemComanda;
	}
	
	public void setListagemComanda(List<Comanda> listagemComanda) {
		this.listagemComanda = listagemComanda;
	}
}