package br.unicesumar.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.unicesumar.dao.impl.ProdutoDAO;
import br.unicesumar.persistence.Bebida;
import br.unicesumar.persistence.Porcao;
import br.unicesumar.types.TipoDeBebida;
import br.unicesumar.types.TipoDePorcao;

@ManagedBean
@SessionScoped
public class ProdutoBean {
	private Bebida bebida = new Bebida();
	private Porcao porcao = new Porcao();
	private String nome;
	private double preco;

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	private List<Bebida> listagemBebidas = new ArrayList<Bebida>();
	private List<Porcao> listagemPorcoes = new ArrayList<Porcao>();

	public String inserirBebida() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			dao.save(bebida);
			bebida = new Bebida();
			context.addMessage(null, new FacesMessage("inserido com sucesso"));
			pesquisarBebida();
		} catch (Exception e) {
			context.addMessage(null,
					new FacesMessage("Erro ao inserir: " + e.getMessage()));
		}
		return "bebidaList";
	}

	public String inserirPorcao() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			dao.save(porcao);
			porcao = new Porcao();
			context.addMessage(null, new FacesMessage("inserido com sucesso"));
			pesquisarBebida();
		} catch (Exception e) {
			context.addMessage(null,
					new FacesMessage("Erro ao inserir: " + e.getMessage()));
		}
		return "porcaoList";
	}

	public Bebida getBebida() {
		return bebida;
	}

	public void setBebida(Bebida bebida) {
		this.bebida = bebida;
	}

	public Porcao getPorcao() {
		return porcao;
	}

	public void setPorcao(Porcao porcao) {
		this.porcao = porcao;
	}

	public List<Porcao> getListagemPorcoes() {
		return listagemPorcoes;
	}

	public void setListagemPorcoes(List<Porcao> listagemPorcoes) {
		this.listagemPorcoes = listagemPorcoes;
	}

	private ProdutoDAO dao = new ProdutoDAO();

	public List<Bebida> getListagemBebidas() {
		return listagemBebidas;
	}

	public void setListagemBebidas(List<Bebida> listagemBebidas) {
		this.listagemBebidas = listagemBebidas;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void pesquisarBebida() {
		listagemBebidas = dao.pesquisarBebida(nome);
	}

	public void pesquisarPorcao() {
		listagemPorcoes = dao.pesquisarPorcao(nome);
	}

	public TipoDeBebida[] getTipoDeBebida() {
		return TipoDeBebida.values();
	}

	public TipoDePorcao[] getTipoDePorcao() {
		return TipoDePorcao.values();
	}

	public String novaBebida() {
		this.bebida = new Bebida();
		return "bebidas";
	}

	public String editarBebida(Bebida bebida) {
		this.bebida = bebida;
		return "bebidas";
	}

	public void guardarSelecaoBebida(Bebida bebida) {
		this.bebida = bebida;
	}

	public String novaPorcao() {
		this.porcao = new Porcao();
		return "porcoes";
	}

	public String editarPorcao(Porcao porcao) {
		this.porcao = porcao;
		return "porcoes";
	}

	public void guardarSelecaoPorcao(Porcao porcao) {
		this.porcao = porcao;
	}
}
