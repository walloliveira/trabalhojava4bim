package br.unicesumar.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.unicesumar.dao.GenericHibernateDao;
import br.unicesumar.persistence.Bebida;
import br.unicesumar.persistence.Porcao;
import br.unicesumar.persistence.Produto;

public class ProdutoDAO extends GenericHibernateDao<Produto, Long> {

	public List<Bebida> pesquisarBebida(String nome) {
		Criteria criteria = getSession().createCriteria(Bebida.class);
		if (nome != "") {
			criteria.add(Restrictions.like("nome", nome, MatchMode.ANYWHERE));
		}
		return criteria.list();
	}

	public List<Porcao> pesquisarPorcao(String nome) {
		Criteria criteria = getSession().createCriteria(Porcao.class);
		if (nome != "") {
			criteria.add(Restrictions.like("nome", nome, MatchMode.ANYWHERE));
			return criteria.list();
		}
		return criteria.list();
	}

}
