package br.unicesumar.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.unicesumar.dao.GenericHibernateDao;
import br.unicesumar.persistence.Mesa;
import br.unicesumar.persistence.Porcao;

public class MesaDAO extends GenericHibernateDao<Mesa, Long> {

	public List<Mesa> pesquisarMesa(String numeroMesa) {
		Criteria criteria = getSession().createCriteria(Mesa.class);
		if (numeroMesa != "") {
			criteria.add(Restrictions.like("numeroMesa", numeroMesa,
					MatchMode.ANYWHERE));
			return criteria.list();
		}
		return criteria.list();
	}

}
